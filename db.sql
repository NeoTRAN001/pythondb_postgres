CREATE DATABASE "Prueba";

CREATE TABLE public.distro
(
  id integer NOT NULL DEFAULT nextval('distro_id_seq'::regclass),
  name text,
  date text
);

INSERT INTO (id, name, date) VALUES (1, 'Debian', '1993');

